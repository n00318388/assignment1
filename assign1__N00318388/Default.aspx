﻿<%@ Page Language="C#" Inherits="assign1__N00318388.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Assignment 1</title>
</head>
<body>
	 <form id="form1" runat="server">
        <div>
            <p>Cleaning Service</p>
            <asp:TextBox runat="server" ID="clientName" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <br />
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <asp:TextBox runat="server" ID="clientAddress" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientAddress" ErrorMessage="Please enter an address"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="cleanTime" placeholder="Appointment Time"></asp:TextBox>
            <asp:RangeValidator EnableClientScript="false" ID="hourscontrol_1" runat="server" ControlToValidate="cleanTime" MinimumValue="800" Type="Integer" MaximumValue="1700" ErrorMessage="Our hours are between 11am and 10pm"></asp:RangeValidator>
            <br />
            <asp:TextBox runat="server" ID="rooms" placeholder="Number of Rooms"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="rooms" Type="Integer" MinimumValue="2" MaximumValue="12" ErrorMessage="Minimun 2 Rooms"></asp:RangeValidator>
            <br />
            <asp:DropDownList runat="server" ID="place">
                <asp:ListItem Value="S" Text="Apartment"></asp:ListItem>
                <asp:ListItem Value="M" Text="House"></asp:ListItem>
                <asp:ListItem Value="L" Text="Business"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:RadioButton runat="server" Text="Paypal" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Credit/Debit" GroupName="via"/>
            <asp:RadioButton runat="server" Text="Check" GroupName="via"/>
            <br />
            <div id="extra" runat="server">
            <asp:CheckBox runat="server" ID="extraService1" Text="Laundry" />
            <asp:CheckBox runat="server" ID="extraService2" Text="Dishes" />
            <asp:CheckBox runat="server" ID="extraService3" Text="Yard Work" />
            </div>
            <br />
            <br />
            <div runat="server" ID="res"></div>
            
        </div>
		<asp:Button id="button1" runat="server" Text="Submit!" OnClick="button1Clicked" />
	</form>
</body>
</html>
